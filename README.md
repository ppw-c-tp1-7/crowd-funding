Anggota Kelompok:
- Grahana Daffa H 1706074700
- Janitra Ariena Sekarputri 1706979316
- Nur Rifandy 1706984695
- Muhammad Anwar Farihin 1706039635

Status Pipeline
[![pipeline status](https://gitlab.com/ppw-c-tp1-7/crowd-funding/badges/master/pipeline.svg)](https://gitlab.com/ppw-c-tp1-7/crowd-funding/commits/master)

Status Code Coverage
[![coverage report](https://gitlab.com/ppw-c-tp1-7/crowd-funding/badges/master/coverage.svg)](https://gitlab.com/ppw-c-tp1-7/crowd-funding/commits/master)

Link Herokuapp
http://ppw-c-7.herokuapp.com/
