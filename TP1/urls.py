"""TP1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from program.views import program
from donasi.views import donasi
from register.views import register,login,logout_
from testimoni.views import page
from profil.views import daftar_donasi


urlpatterns = [
    path('', include('program.urls'),name='home'),
    path('program/', program, name='program'),
    path('admin/', admin.site.urls),
    path('news/',include('newsApp.urls'),name='news'),
    path('donasi/', donasi, name='donasi'),
    path('register/', register, name='register'),
    path('login/', login, name='login'),
    path('logout/', logout_, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    path('testimoni/', include('testimoni.urls'), name='testimoni'),
    path('testimoni/',page, name='testimoni'),
    path('profil/',daftar_donasi, name='profil'),
    
]
