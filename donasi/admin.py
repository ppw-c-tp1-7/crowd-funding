from django.contrib import admin

# Register your models here.
from .models import DonationSubmission
admin.site.register(DonationSubmission)