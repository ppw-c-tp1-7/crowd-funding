from django import forms
from .models import DonationSubmission
from program.models import Program,Program2, ListProgram


class DonationForm(forms.ModelForm):
    attrs_program = {
        'class': 'form-control',
        'size': 200,
        'placeholder': 'Masukkan program yang ingin Anda pilih'

    }
    attrs_fullname = {
        'class': 'form-control',
        'size': 7,
        'placeholder': 'Masukkan nama lengkap Anda'
    }
    attrs_email = {
        'class': 'form-control',
        'size': 7,
        'placeholder': 'Masukkan email valid Anda'
    }
    attrs_amount = {
        'class': 'form-control',
        'size': 7,
        'placeholder': 'Masukkan jumlah dana yang ingin Anda donasikan'
    }

    CHOICES = [(x.programTittle,x.programTittle) for x in ListProgram.objects.all()]

    program = forms.ChoiceField(label='PROGRAM',choices=CHOICES)
    fullname = forms.CharField(label='NAMA LENGKAP', required=True, widget=forms.widgets.TextInput(attrs=attrs_fullname))
    email = forms.EmailField(label='E-MAIL', required=True, widget=forms.widgets.EmailInput(attrs=attrs_email))
    amount = forms.CharField(label='JUMLAH DONASI', required=True, widget=forms.widgets.NumberInput(attrs=attrs_amount))
    is_anon = forms.BooleanField(widget=forms.CheckboxInput(), required=False, label="DONASI SEBAGAI ANONIM",initial = False)
    class Meta:
        model = DonationSubmission
        fields = ['program','fullname','email','amount','is_anon']