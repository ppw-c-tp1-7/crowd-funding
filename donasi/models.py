from django.db import models
from datetime import datetime
# Create your models here.

class DonationSubmission(models.Model):
    program = models.CharField(max_length=100)
    fullname = models.CharField(max_length=50)
    email = models.EmailField(max_length=30)
    amount = models.IntegerField(default = 0)
    is_anon = models.BooleanField(default = False)
    time = models.DateTimeField(default=datetime.now,null = True)

    def __str__(self):
        return '%s, %s, %s' % (self.program, self.fullname,self.amount )
