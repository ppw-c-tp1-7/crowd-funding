from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve
from donasi.views import donasi
from donasi.models import DonationSubmission
from donasi.forms import DonationForm
from register.models import Register
from django.http import HttpRequest
from django.urls import resolve
from datetime import datetime

#Create your tests here.
class TP1Test(TestCase):
    #routing program benar
    def test_donasi_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    #program meng extends base.html
    def test_donasi_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

    def test_donasi_function(self):
        found = resolve('/donasi/')
        self.assertEqual(found.func, donasi)


    def test_model_can_create_objects(self):
        DonationSubmission.objects.create(fullname='anwar farihin')
        counting_all_available_status = DonationSubmission.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    # def test_donate_can_check_email_has_registed_before(self):
    #     Register.objects.create(email = "farihin@google.com",birth_date = datetime.now() )
    #     DonationSubmission.objects.create(email = "fari@google.com")
    #     counting_all_registed_donations = DonationSubmission.objects.all().count()
    #     self.assertEqual(counting_all_registed_donations, 0)


    def test_form_validation_for_blank_items(self):
        form = DonationForm(data={'form': ''})
        self.assertFalse(form.is_valid())

    ##login tp2 by farihin

    # def test_login_has_session(self):
    #     c = Client()
    #     c.force_login(User.objects.get_or_create(username='testuser')[0])
    #     home = c.get('/')
    #     self.assertTrue(home.context['user'].is_authenticated)
    #     session = dict(c.session)
    #     self.assertTrue("username" in session)
    #     self.assertTrue("email" in session)
    #     self.assertTrue("name" in session)
    #     response = c.get('/')
    #     self.assertIn("Log Out", response.content.decode('utf8'))

    # def test_logout(self):
    #     c = Client()
    #     c.force_login(User.objects.get_or_create(username='testuser')[0])
    #     c.logout()
    #     home = c.get('/')
    #     self.assertFalse(home.context['user'].is_authenticated)
    #     session = dict(c.session)
    #     self.assertFalse("username" in session)
    #     self.assertFalse("email" in session)
    #     self.assertFalse("name" in session)
    #     response = c.get('/')
    #     self.assertIn("Daftar Donatur", response.content.decode('utf8'))

    #link
    #functional dr setiap fitur
    # bisa submit gak
    # kalo input jelek
    # email kalo salah gimana
    #