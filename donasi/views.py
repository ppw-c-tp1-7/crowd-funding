from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import DonationForm
from .models import DonationSubmission
from register.models import Register
from program.models import Program,Program2
from . import forms

def donasi(request):
    response = {}

    users = Register.objects.all()

    if request.method == 'POST':
        form = forms.DonationForm(request.POST)
        if form.is_valid():
            for user in users:
                if user.email == request.POST['email']:
                    data = form.cleaned_data
                    DonationSubmission.objects.create(**data)
                    return render(request, 'baseDonasi.html', {'form': form, 'done':'terima kasih ' + request.POST['fullname'].upper() +' untuk donasinya:)', })
                else:
                    print("Email doesn't exists")
                    # if request.POST['is_anon']:
                    #     name = "Anonym"
                    # else:
                    #     name = request.POST.get('fullname')
                    # form.get('name') = name
                    # form.save()
                    
            return render(request, 'baseDonasi.html', {'form': form, 'message':'e-mail ' + request.POST['email'] +' belum terdaftar', })


    else:
        if request.user.is_authenticated:
            if 'username' not in request.session:
                 request.session['username'] = request.user.username
            if 'email' not in request.session:
                 request.session['email'] = request.user.email
            if 'name' not in request.session:
                 request.session['name'] = request.user.first_name + " " + request.user.last_name


            print(request)
            print(request.session['email'])
            print(request.user.email)
        else:
             print('cant donate before login. redirecting to login form...')
             messages.warning(request, 'Please login below before donate')
             return redirect('/login')
        form = DonationForm()
    return render(request, 'baseDonasi.html', {'form': form, },response)
