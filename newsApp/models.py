from django.db import models

# Create your models here.
class News(models.Model):
	img_news = models.CharField(max_length=500)
	title = models.CharField(max_length=200)
	author = models.CharField(max_length=200)
	main_description = models.TextField()
	full_description = models.TextField()
	created_date = models.DateField(auto_now_add=True)

def __str__(self):
	return self.title