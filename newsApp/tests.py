from django.test import TestCase
from django.test import Client
from django.urls import resolve
from newsApp.views import *
from django.http import HttpRequest
from unittest import skip

# Create your tests here.
class testAwesomeWebsite(TestCase):

	def test_html_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_news_using_index_template(self):
		response = Client().get('/news/')
		self.assertTemplateUsed(response, 'base.html')

	def test_news_function(self):
		found = resolve('/news/')
		self.assertEqual(found.func, news)
	
	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = news(request)
		html_response = response.content.decode('utf8')
		self.assertIn('', html_response)

	def test_model_can_create_objects(self):
		News.objects.create(title='Gempa Bumi Situbondo')
		counting_all_available_status = News.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)