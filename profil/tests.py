from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import daftar_donasi
import unittest
import time

# Create your tests here.
class Profil_Test(TestCase):
    def test_profil_url_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 200)

    def test_profil_using_login_template(self):
        response = Client().get('/profil/')
        self.assertTemplateUsed(response, 'profil.html')

    def test_profil_using_indexProfil_func(self):
        found = resolve('/profil/')
        self.assertEqual(found.func, daftar_donasi)