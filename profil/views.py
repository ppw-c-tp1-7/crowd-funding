from django.shortcuts import render
from donasi.models import DonationSubmission
from django.contrib.auth.models import User
from django.contrib.auth import authenticate



# Create your views here.

response = {}
def daftar_donasi(request):
    if request.user.is_authenticated:
        email = request.user.email
        daftar_donasi = DonationSubmission.objects.filter(email=email)
        totalDonasi = 0
        for i in daftar_donasi:
            totalDonasi += int(i.amount)
    
        response = {
            'daftar_donasi' : daftar_donasi,
            'totalDonasi' : totalDonasi,
        }
        return render(request, "profil.html", response)

    else:
        return render(request, "profil.html")