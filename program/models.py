from django.db import models

# Create your models here.
class Program(models.Model):
    image = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    donatur = models.CharField(max_length=200)

class Program2(models.Model):
    image = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    donatur = models.CharField(max_length=200)

class ListProgram(models.Model):
	urlProgram = models.CharField(max_length=50)
	targetDonasi = models.IntegerField()
	donasiTerkumpul = models.IntegerField()
	imageUrl = models.CharField(max_length=200)
	publisher = models.CharField(max_length=200)
	programTittle = models.CharField(max_length=200)
	tanggalBerakhir = models.DateField(auto_now=True)
	description = models.TextField()
	checkingGoals = models.BooleanField()