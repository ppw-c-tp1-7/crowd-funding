from django.test import TestCase
from django.test import Client
from program.views import index, program
from django.http import HttpRequest
from django.urls import resolve
from .models import Program

# Create your tests here.

class programTest(TestCase):
        def testUrl(self):
                c = Client()
                response = c.get('/program/')
                response2 = c.get('/')
                self.assertTrue(response.status_code, 200)
                self.assertTrue(response2.status_code, 200)

        def testContent(self):
                self.assertIsNotNone(index)
                self.assertIsNotNone(program)

        def testContain(self):
                request = HttpRequest()
                response = index(request)
                html_response = response.content.decode('utf8')
                self.assertIn('<title>Donat.in</title>', html_response)

        def testContain2(self):
                request = HttpRequest()
                response = program(request)
                html_response = response.content.decode('utf8')
                self.assertIn('<title>Program Donasi</title>', html_response)

        def testFunction(self):
                found = resolve('/program/')
                self.assertEqual(found.func , program)

        def testFunction2(self):
                found = resolve('/')
                self.assertEqual(found.func , index)

        def testTemplate(self):
                response = Client().get('/program/')
                self.assertTemplateUsed(response, 'program/program_donasi.html')

        def testTemplate2(self):
                response = Client().get('/')
                self.assertTemplateUsed(response, 'program/main.html')

        def testCanCreateDatabase(self):
                new_program = Program.objects.create(description="test program")
                allprogram = Program.objects.all().count()
                self.assertEqual(allprogram,1)
