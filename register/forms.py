from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from .models import Register


class Regist_Form(forms.Form):
    attrs_name = {
        'class': 'form-control',
        'size': 7,
        'placeholder': 'Masukkan nama lengkap Anda'
    }
    attrs_date = {
        'class': 'form-control',
        'size': 7,
        'type': 'date'
    }
    attrs_email = {
        'class': 'form-control',
        'size': 7,
        'placeholder': 'Masukkan email valid Anda'
    }
    attrs_pass = {
        'class': 'form-control',
        'size': 7,
        'placeholder': 'Masukkan kata sandi Anda'
    }
    attrs_confirm = {
        'class': 'form-control',
        'size': 7,
        'placeholder': 'Konfirmasi kata sandi Anda'
    }

    name = forms.CharField(label='NAMA LENGKAP', required=True, widget=forms.widgets.TextInput(attrs=attrs_name))
    birth_date = forms.DateField(label='TANGGAL LAHIR', required=True, widget=forms.widgets.DateInput(attrs=attrs_date))
    email = forms.EmailField(label='EMAIL', required=True, widget=forms.widgets.EmailInput(attrs=attrs_email))
    password1 = forms.CharField(label='PASSWORD', required=True, min_length=8, max_length=12, validators=[RegexValidator('^(\w+\d+|\d+\w+)+$', message="Kata sandi harus mengandung kombinasi alfabet dan angka")], widget=forms.PasswordInput(attrs=attrs_pass))
    password2 = forms.CharField(label='CONFIRM PASSWORD', required=True, min_length=8, max_length=12, widget=forms.PasswordInput(attrs=attrs_confirm))

    def clean_name(self):
        name = self.cleaned_data['name'].lower()
        r = Register.objects.filter(name=name)
        if r.count():
            raise ValidationError("Anda telah terdaftar sebelumnya")

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = Register.objects.filter(email=email)
        if r.count():
            raise  ValidationError("Email Anda telah terdaftar sebelumnya")

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Kata sandi Anda tidak cocok")
