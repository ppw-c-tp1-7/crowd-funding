from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Register(models.Model):
    name = models.CharField(max_length=100, unique=True)
    birth_date = models.DateField()
    email = models.EmailField(max_length=50, unique=True, default=False)
    password1 = models.CharField(max_length=12)
    password2 = models.CharField(max_length=12)
