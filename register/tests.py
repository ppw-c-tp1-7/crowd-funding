from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import register
from .models import Register
from .forms import Regist_Form
import unittest

# Create your tests here.
class Register_Test(TestCase):
    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_using_to_do_list_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'pendaftaran.html')

    def test_register_using_pendaftaran_func(self):
        found= resolve('/register/')
        self.assertEqual(found.func, register)

    def test_model_can_create_new_user(self):
        new_user = Register.objects.create(name="a", birth_date="1999-10-16", email="anit@yahoo.com", password1="hello", password2="hello")
        counting_all_user = Register.objects.all().count()
        self.assertEqual(counting_all_user, 1)
