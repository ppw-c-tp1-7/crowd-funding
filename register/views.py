from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import IntegrityError
from .forms import Regist_Form
from .models import Register
from program.views import index, program
from django.contrib.auth import logout


# Create your views here
def register(request):
    reg_form = Regist_Form(request.POST or None)
    regist = Register.objects.all()
    response = {}
    response['reg_form'] = reg_form
    response['regist'] = regist
    try:
        if request.method == 'POST' and reg_form.is_valid():
            name = request.POST.get("name")
            birth_date = request.POST.get("birth_date")
            email = request.POST.get("email")
            password1 = request.POST.get("password1")
            password2 = request.POST.get("password2")

            model = Register(name=name, birth_date=birth_date, email=email, password1=password1, password2=password2)
            model.save()
            return redirect('program')
        else:
            return render(request, 'pendaftaran.html', response)
    except IntegrityError as e:
        return render(request, 'pendaftaran.html', response)


def login(request):
    return render(request, 'login.html')

def logout_(request):
    logout(request)
    return redirect('/')
