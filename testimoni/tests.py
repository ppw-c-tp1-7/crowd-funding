from django.test import TestCase, Client
from testimoni.views import page
from django.http import HttpRequest
from django.urls import resolve
# Create your tests here.

class testimoniTest(TestCase):
	def testUrl(self):
		c = Client()
		response = c.get('/testimoni/')
		self.assertTrue(response.status_code, 200)

	def testContent(self):
		self.assertIsNotNone(page)

	def testContain(self):
		request = HttpRequest()
		response = page(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Testimoni</title>', html_response)

	def testFunc(self):
		found = resolve('/testimoni/')
		self.assertEqual(found.func, page)

	def testTemplate(self):
		response = Client().get('/testimoni/')
		self.assertTemplateUsed(response, 'page.html')