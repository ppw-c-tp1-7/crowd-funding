from django.shortcuts import render
from .models import Testimoninya
from .forms import formTestimoni
from django.http import JsonResponse, HttpResponse
import requests, json, re
# def page(request):
# 	testimonies = Testimoninya.objects.all()
# 	response = {"testimoninya" : testimonies}
# 	return render(request, 'page.html', response)

def page(request):
	response = {}
	response2 = {}
	testimonies = Testimoninya.objects.all()
	response = {"testimoninya" : testimonies, "form" : formTestimoni}
	form = formTestimoni(request.POST or None)
	if(request.method == "POST"):
		if(form.is_valid()):
			text_form = request.POST.get("text_form")
			Testimoninya.objects.create(textTestimoni=text_form, nameTestimoni=request.user.username,)
			response2['text'] = text_form
			response2['name'] = request.user.username
			# return render(request, 'page.html', response2)
			return HttpResponse(json.dumps(response2), content_type='application/json')
		else:
			return render(request, 'page.html', response)
	else:
		response['form'] = form
		return render(request, 'page.html', response)

def getData(request):
	all_data = Testimoninya.objects.all().values()
	testimonies = list(all_data)
	return JsonResponse({'all_data' : testimonies})